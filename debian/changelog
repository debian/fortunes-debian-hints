fortunes-debian-hints (2.01.6) unstable; urgency=low

  * po4a/po:
    + pt.po: Updated Portuguese translation, Thanks to Américo Monteiro.
      (Closes: #1056002)
    + de.po: Updated German translation, Thanks to Helge Kreutzmann.
      (Closes: #1055997)
    + nl.po: Added Dutch translation, Thanks to Frans Spiesschaert.
      (Closes: #1059566)
    + zh_CN.po, zh_TW.po: Updated Chinese translations, Thanks to
      Yangfl <mmyangfl@gmail.com>.

 -- Kartik Mistry <kartik@debian.org>  Thu, 08 Feb 2024 13:28:13 +0530

fortunes-debian-hints (2.01.5) unstable; urgency=low

  [ Kartik Mistry ]
  * debian/control:
    + Updated Standards-Version to 4.6.2
  * Moved po4a from Build-Depends-Indep to Build-Depends (Closes: #1021218)

  [ Guillem Jover ]
  * Updated hints.

 -- Kartik Mistry <kartik@debian.org>  Wed, 15 Nov 2023 10:31:44 +0530

fortunes-debian-hints (2.01.4) unstable; urgency=low

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends-Indep: Drop versioned constraint on po4a.
    + fortunes-debian-hints: Drop versioned constraint on fortune-mod in
      Recommends.

 -- Kartik Mistry <kartik@debian.org>  Thu, 02 Sep 2021 09:49:49 +0530

fortunes-debian-hints (2.01.3) unstable; urgency=low

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Apply multi-arch hints.
    + fortunes-debian-hints: Add Multi-Arch: foreign.

  [ Kartik Mistry ]
  * debian/control:
    + Updated Standards-Version to 4.5.1
    + Use debhelper-compat to 13.
  * Added debian/gitlab-ci.yml.
  * debian/rules:
    + Use dh sequence.

 -- Kartik Mistry <kartik@debian.org>  Thu, 24 Dec 2020 12:02:19 +0530

fortunes-debian-hints (2.01.2) unstable; urgency=medium

  [ Kartik Mistry ]
  * Unfuzzy po files for last change (Closes: #901893)
  * debian/control:
    + Updated to Standards-Version 4.1.4

  [ Guillem Jover ]
  * debian/:
    + Add a new update-po convenience target
    + Refactor po4a invocation
    + Set Rules-Requires-Root to no
    + Cleanup debian/rules targets
    + Fix Vcs-Browser URL
  * Mention unattended-upgrades as an alternative to cron-apt
  * Clarify sysvinit service disabling
  * Switch from the PTS to the new Package Tracker
  * Mention new manpages.debian.org service
  * Update backports usage reference
  * Update apt tooling usage
  * Switch URLs to https
  * Remove executable bits from non-executable files

 -- Kartik Mistry <kartik@debian.org>  Wed, 20 Jun 2018 09:23:51 +0530

fortunes-debian-hints (2.01.1) unstable; urgency=low

  * New release, minor changes in debian directory only.
  * debian/control:
    + Updated to Standards-Version 4.1.3.
    + Updated Vcs-* URLs.
  * Bumped dh to 11.
  * debian/copyright:
    + Updated URLs: http->https.

 -- Kartik Mistry <kartik@debian.org>  Wed, 17 Jan 2018 12:37:45 +0530

fortunes-debian-hints (2.01) unstable; urgency=low

  * New release.
  * Updated Portuguese translation (Closes: #755130)
  * debian/control:
    + Updated Standards-Version to 3.9.6
  * debian/copyright:
    + Updated copyright and field license short-name.

 -- Kartik Mistry <kartik@debian.org>  Fri, 10 Jul 2015 23:35:44 +0530

fortunes-debian-hints (2.00) unstable; urgency=low

  * New release.
  * Added Arabic (ar) translation. Thanks to Mohamed Amine <med@mailoo.org>.
  * Updated Simplified Chinese translation (zh_CN). Thanks to Vern Sun
    <s5unty@gmail.com>.
  * Applied wrap-and-sort magic.
  * debian/control:
    + Fixed VCS-* URLs.
    + Updated Standards-Version to 3.9.4
  * debian/copyright:
    + Updated to copyright-format 1.0

 -- Kartik Mistry <kartik@debian.org>  Mon, 30 Sep 2013 17:18:27 +0530

fortunes-debian-hints (1.99) unstable; urgency=low

  [Kartik Mistry]
  * New release:
  * hints:
    + Fixed hint #27 with much better text by Antoine Beaupré
      <anarcat@koumbit.org>.
    + Wrapped up hint #40.
  * Bumped debian/compat to 7.
  * po4a/de.po: Updated German translation, Thanks to Helge Kreutzmann
    <debian@helgefjell.de> (Closes: #649934).
  * po4a/ja.po: Added Japanese translation, Thanks to KURASAWA Nozomu
    <nabetaro@debian.or.jp> (Closes: #650643).
  * debian/copyright:
    + Updated copyright year.
    + Updated DEP-5 format URL.

  [Antoine Beaupré]
  * po4a/*.po: Refreshed for updated hints.
  * po4a/fr.po: Updated French translation.
  * No need for strfile, we build-depend on fortune.

 -- Kartik Mistry <kartik@debian.org>  Mon, 06 Feb 2012 23:07:08 +0530

fortunes-debian-hints (1.98) unstable; urgency=low

  * New release:
    + Fixed hint #36 for stable-updates and backports.debian.org, Thanks to
      Geoff Simmons <gsimmons@gsimmons.org> for patch. (Closes: #636234)
    + Replace news.d.n with DPN. Add hint for Identi.ca, Patch from
      Fernando C. Estrada <fcestrada@fcestrada.com>. (Closes: #641862)
  * po4a/po:
    + cs.po: Updated Czech translation, Thanks to Miroslav Kure
      <kurem@upcase.inf.upol.cz>. (Closes: #642979)
    + de.po: Updated German translation. (Closes: #641959)
    + es.po: Updated Spanish translation, Thanks to Francisco Javier Cuadrado
      <fcocuadrado@gmail.com>.
    + fr.po: Updated French translation, Thanks to Steve Petruzzello
      <dlist@bluewin.ch>. (Closes: #646784)
    + pt.po: Updated Portuguese translation, Thanks to Américo Monteiro
      <a_monteiro@netcabo.pt>.
    + sv.po: Updated Swedish translation, Thanks to Martin Ågren
      <martin.agren@gmail.com>.
    + zh_TW.po: Updated Triditional Chinese translation, Thanks to Rex Tsai
      <chihchun@kalug.linux.org.tw>.
  * debian/control:
    + Updated Standards-Version to 3.9.2
  * debian/rules:
    + Added missing build targets.
  * debian/copyright:
    + Updated to latest DEP5 format.

 -- Kartik Mistry <kartik@debian.org>  Wed, 09 Nov 2011 11:44:05 +0530

fortunes-debian-hints (1.97) unstable; urgency=low

  * po4a/po/es.po:
    + Updated Spanish translation, Thanks to Francisco Javier Cuadrado
      <fcocuadrado@gmail.com>
  * po4a/po/pt.po:
    + Updated Portuguese translation, Thanks to Américo Monteiro
      <a_monteiro@netcabo.pt> (Closes: #603740)
  * po4a/po/sv.po:
    + Updated Swedish translation, Thanks to Martin Ågren
      <martin.agren@gmail.com> (Closes: #604838)
  * po4a/po/it.po:
    + Updated Italian translation, Thanks to Vincenzo Campanella
      <vinz65@gmail.com> (Closes: #603658)

 -- Kartik Mistry <kartik@debian.org>  Thu, 25 Nov 2010 18:03:02 +0530

fortunes-debian-hints (1.96) unstable; urgency=low

  * New release:
    + Fixed URL in hint #14, Thanks to Steve Petruzzello <dlist@bluewin.ch>
      (Closes: #596934)
    + Unfuzzy strings for #14 change for all .po files
  * po4a/po/fr.po:
    + Updated French translation, Thanks to Steve Petruzzello
      <dlist@bluewin.ch> (Closes: #602930)
  * po4a/po/de.po:
    + Updated German translation, Thanks to Helge Kreutzmann
      <debian@helgefjell.de> (Closes: #603302)
  * po4a/po/cs.po:
    + Updated Czech translation, Thanks to Miroslav Kure
      <kurem@upcase.inf.upol.cz> (Closes: #603442)
  * debian/control:
    + Updated Standards-Version to 3.9.1

 -- Kartik Mistry <kartik@debian.org>  Sun, 14 Nov 2010 11:45:23 +0530

fortunes-debian-hints (1.95) unstable; urgency=low

  * New release:
    + Fixed hints and typos in 6, 11, 26, 35. Thanks to Helge Kreutzmann
      <debian@helgefjell.de> (Closes: #571148)
    + Fixed URL in hint 25
    + TODO: Rewrite hints: 27 and 31
  * Note that Slovak (sk) translation was added with 1.93 release
  * debian/copyright:
    + Updated as per DEP-5 specification
    + Do not point to BSD in common-licenses
  * debian/README.Debian:
    + Wrapped up to 80 characters
  * debian/control:
    + Updated Standards-Version to 3.9.0

 -- Kartik Mistry <kartik@debian.org>  Mon, 19 Jul 2010 22:22:26 +0530

fortunes-debian-hints (1.94) unstable; urgency=low

  * New release:
    + Package is under collab-maint git repository now
    + Using new source format 3.0 (native)
    + Fixed hints numbers
    + Fixed hints: 1, 3, 9, 29
    + Added hint: 43
  * po4a/po/cs.po:
    + Updated Czech translation. Thanks to Miroslav Kure
      <kurem@upcase.inf.upol.cz> (Closes: #570748)
  * po4a/po/de.po:
    + Updated German translation. Thanks to Helge Kreutzmann
      <debian@helgefjell.de> (Closes: #571147)
  * po4a/po/es.po:
    + Updated Spanish translation. Thanks to Francisco Javier Cuadrado
      <fcocuadrado@gmail.com>
  * po4a/po/fr.po:
    + Updated French translation. Thanks to Steve Petruzzello
      <dlist@bluewin.ch> and debian-l10n-french team (Closes: #571200)
  * po4a/po/it.po:
    + Updated Italian translation. Thanks to Vincenzo Campanella
      <vinz65@gmail.controlm>
  * po4a/po/pt.po:
    + Updated Portuguese translation. Thanks to Américo Monteiro
      <a_monteiro@netcabo.pt> (Closes: #570130)
  * po4a/po/sv.po:
    + Updated Swedish translators. Thanks to Martin Ågren
      <martin.agren@gmail.com>
  * po4a/po/tr.po:
    + Updated Turkish translation. Thanks to İsmail Baydan
      <ibaydan@gmail.com>
  * po4a/po/vi.po:
    + Updated Vietnamese translation. Thanks to Clytie Siddall
      <clytie@riverland.net.au>
  * po4a/po/zh_CN.po:
    + Updated Simplified Chinese translation. Thanks to soyi sept
      <sept.soyi@gmail.com>
  * po4a/po/zh_TW.po:
    + Updated Traditional Chinese translation. Thanks to Rex Tsai
      <chihchun@kalug.linux.org.tw>
  * debian/control:
    + Updated Standards-Version to 3.8.4
    + Added ${misc:Depends} to Depends field as we use debhelper
    + Added VCS-* fields
  * debian/rules:
    + Better clean and install targets
  * debian/copyright:
    + Updated copyright year
  * debian/TODO:
    + Since, we have better and sane way of using translations, removed it
      from TODO
  * Disabled hu.po, since po4a needs >= 80% translation. However,
    po4a/po/hu.po is kept is source

 -- Kartik Mistry <kartik@debian.org>  Sun, 07 Mar 2010 16:36:55 +0530

fortunes-debian-hints (1.93) unstable; urgency=low

  * New release:
    + Added 5 more hints and typo in hint #38, Thanks to all contributors!
    + po4a/po/cs.po:
      Added Czech (cs) translation, Thanks to Miroslav Kure
      <kurem@upcase.inf.upol.cz>
    + po4a/po/es.po:
      Added Spanish (es) translation, Thanks to Francisco Javier Cuadrado
      <fcocuadrado@gmail.com> (Closes: #557617)
    + po4a/po/it.po:
      Added Italian (it) translation, Thanks to Vincenzo Campanella
      <vinz65@gmail.com> (Closes: #557908)
    + po4a/po/sv.po:
      Added Swedish translation, Thanks to Martin Ågren
      <martin.agren@gmail.com>
    + Updated German (de) translation (Closes: #558157)
    + Updated Portuguese (pt) translation (Closes: #558374)
    + Updated Traditional Chinese (zh_TW) translation
  * debian/control:
    + Updated Standards-Version to 3.8.3
    + Updated debhelper dependency to 7
    + Wrapped up Build-Depends
  * debian/rules:
    + Fixed my email ID in PO file header generation
    + Used dh_prep instead of dh_clean -k
  * debian/TODO:
    + Added note that we need more translations
  * debian/copyright:
    + Updated copyright year for package

 -- Kartik Mistry <kartik@debian.org>  Sun, 13 Dec 2009 20:28:49 +0530

fortunes-debian-hints (1.92) unstable; urgency=low

  * New release:
    + po4a/po/hints.pot:
      Really fixed wrong URL of hints #14
    + po4a/po/*.po:
      Updated for hints #14 change

 -- Kartik Mistry <kartik@debian.org>  Thu, 18 Sep 2008 16:28:07 +0530

fortunes-debian-hints (1.91) unstable; urgency=low

  * New release:
    + po4a/po/hints.pot:
      Fixed wrong URL in hints #14, Thanks to Rex Tsai for patch
      <chihchun@kalug.linux.org.tw>, Updated my email address
    + po4a/po/zh_TW.po:
      Added Traditional Chinese, Thanks to Rex Tsai (Closes: #497704)
    + po4a/po/zh_CN.po:
      Updated Simplified Chinese (zh_CN) translation
      for typos, Thanks to Rex Tsai for patch (Closes: #497705)
    + po4a/po/fr.po:
      Updated French translation, Thanks to Jean Guillou <guillou.jean@free.fr>
    + po4a/po/de.po:
      Updated German translation, Thanks to Helge Kreutzmann
      <debian@helgefjell.de>
    + po4a/po/*.po:
      Updated my email address
  * debian/control:
    + Updated my maintainer email address
  * debian/README.Debian:
    + Updated my maintainer email address
    + Updated text for translation update reference
  * debian/copyright:
    + Updated license text according to proposed license format
    + Updated my maintainer email address
  * debian/rules:
    + Updated my email for po4a configuration
    + Updated for addition of zh_TW translation

 -- Kartik Mistry <kartik@debian.org>  Tue, 16 Sep 2008 21:58:46 +0530

fortunes-debian-hints (1.9) unstable; urgency=low

  * New release:
    + Added po4a support to use po based translation, Thanks to
      Nicolas Francois <nicolas.francois@centraliens.net> for help
    + Fixed hints #23 to remove obsolete use of debfoster, Thanks
      to Francesco Potorti` <Potorti@isti.cnr.it> (Closes: #484020)
    + Updated German translation, Thanks to Helge Kreutzmann
      <debian@helgefjell.de>
    + Added Hungarian translation, Thanks to SZERVÁC Attila
      <sas@321.hu> (Closes: #485153)
    + Added Vietnamese translation, Thanks to Clytie Siddall
      <clytie@riverland.net.au>
    + Added Simplified Chinese translation, Thanks to Vern Sun
      <s5unty@gmail.com>
    + Added Portuguese translation, Thanks to Traduz - Portuguese
      Translation Team <traduz@debianpt.org> (Closes: #486008)
    + Added French translation, Thanks to jean <guillou.jean@free.fr>
      (Closes: #485907)
    + Added Turkish translation, Thanks to Mert Dirik <mertdirik@gmail.com>
      (Closes: #487235)
  * debian/control:
    + Updated Standards-Version to 3.8.0 (no changes needed)
  * debian/rules:
    + Updated for newly added translations
    + Fixed clean target to remove *.dat files
  * debian/fortunes-debian-hints.install, debian/fortunes-debian-hints.dirs,
    debian/fortunes-debian-hints.links:
    + Updated for newly added translations
  * Renamed todo file to TODO

 -- Kartik Mistry <kartik.mistry@gmail.com>  Mon, 23 Jun 2008 21:39:53 +0530

fortunes-debian-hints (1.8) unstable; urgency=low

  * New release
    + Added 7 new hints: 32 to 38, Thanks to Alexander Schmehl, Kapil
      and others for help
    + Removed obsolate hint: 23
    + Removed obsolate use of aptitude in hint 24, Thanks to Paul Wise
      (Closes: #469134)
    + Updated 3 hints: 1, 6 and 11
    + Added internationalization support, Thanks to Alexander Schmehl
      for patch (Closes: #243721)
  * New maintainer (Closes: #465936)
  * debian/control:
    + Fixed spelling mistake in description (Closes: #363417)
    + Updated to Standards-Version 3.7.3 (no changes needed)
    + Updated debhelper compatibility to 5
    + Moved fortune-mod to Build-Depends-Indep
    + Added Debian wiki page as homepage
  * debian/rules:
    + Removed useless binary-arch target's echo message
  * debian/README.Debian:
    + Updated information to use .bashrc from Alexander Schmehl's patch
    + Added time-stamp and formatted to standard README.Debian file
  * debian/TODO:
    + Updated for adding more hints
  * debian/copyright:
    + Added License section
    + Removed comment like portion from license text
    + Added myself as copyright owner

 -- Kartik Mistry <kartik.mistry@gmail.com>  Sun, 16 Mar 2008 10:00:26 +0530

fortunes-debian-hints (1.7) unstable; urgency=low

  * Fixed two minor spelling / typo errors (Closes: #284314)
  * Updated hint #23 (kernel module configs) to include information
    about Linux 2.6 kernels and modprobe, as well as the old
    information. (Closes: #268371)

 -- Joel Aelwyn <fenton@debian.org>  Tue,  4 Jan 2005 12:09:56 -0700

fortunes-debian-hints (1.6) unstable; urgency=low

  * Added 8 hints. (Closes: #244670, #263993)
  * Updated 7 hints. (Closes: #244669, #276382)
  * Updated maintainer name (same person, just a legal name change).
  * Changed license from GPL-2 to BSD-style.
  * Switched from Build-Depends-Indep to Build-Depends, since we're a
    purely Indep package; it's a stupid requirement, but until it is
    fixed, this is the easiest way to deal with it.

 -- Joel Aelwyn <fenton@debian.org>  Wed, 13 Oct 2004 10:10:47 -0600

fortunes-debian-hints (1.5) unstable; urgency=low

  * Added 1 hint. (Closes: #213973)
  * Upgraded to Policy 3.6.1
  * Correctly purge CVS files on release. Oops.

 -- Joel Baker <fenton@debian.org>  Mon,  6 Oct 2003 22:16:26 -0600

fortunes-debian-hints (1.4) unstable; urgency=low

  * Added 'Provides: fortune-cookie-db' so that installation of other
    fortune DBs isn't necessary. (Closes: #193506)
  * Upgraded to Policy 3.6.0
  * Switched to using the UTF-8 fortune file format.

 -- Joel Baker <fenton@debian.org>  Mon, 21 Jul 2003 14:04:50 -0600

fortunes-debian-hints (1.3) unstable; urgency=low

  * Upgraded to Policy 3.5.9.0
  * Versioned Build-Dep on debhelper (to >= 4).

 -- Joel Baker <fenton@debian.org>  Wed, 14 May 2003 12:20:44 -0600

fortunes-debian-hints (1.2) unstable; urgency=low

  * Fixed hint #4 to reflect the fact that apt-cache policy can handle
    any available package, not just those installed. Thanks to Alexander
    Schmehl for catching it.
  * Clarified preferred procedure for submitting new hints (file a
    wishlist bug).
  * Added 9 hints. (Closes: #191384, #192031, #192032)
  * Moved README to README.Debian, and added a TODO file.

 -- Joel Baker <fenton@debian.org>  Mon, 12 May 2003 12:40:30 -0600

fortunes-debian-hints (1.1) unstable; urgency=low

  * Fixed hint #7 to refer to cron-apt instead of auto-apt.
    (Closes: #185529)
  * Added 3 hints.

 -- Joel Baker <fenton@debian.org>  Wed, 19 Mar 2003 22:45:30 -0700

fortunes-debian-hints (1.0) unstable; urgency=low

  * Initial packaging. (closes: #182617)
  * Added 11 hints.
  * Added example fragment for bash.

 -- Joel Baker <fenton@debian.org>  Sun,  9 Mar 2003 22:31:39 -0700
